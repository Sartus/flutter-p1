

import 'package:flutter/material.dart';
import 'package:pet/common/commmon.dart';
import 'package:pet/dao/pet_pedigree_dao.dart';
import 'package:pet/model/pet_pedigree_model.dart';

class PetPedigreeController {
  final PetPedigreeDao _pedigreeDao;
  final List<PetPedigreeModel> _pedigreeList  = [];
  // final Widget _pedigreeGui;
  // final EnumSideType _type;

  PetPedigreeController({EnumSideType type = EnumSideType.client, Widget gui}) :
    // _type = type,
    _pedigreeDao = PetPedigreeDao(type: type);
    // _pedigreeGui = gui;
  
  Future<List<PetPedigreeModel>> getAddressList () async { 
    return _pedigreeList.isEmpty? getAllRows() : _pedigreeList;
  }

  Future<int> insert(PetPedigreeModel obj) async {
    // result é o conteudo da PK da linha inserida.
    int result;
    try {
      result = await _pedigreeDao.insert(obj);
    } catch(e) {
      return 0;
    }
    _pedigreeList.add(obj);
    return result;
  }

  Future<int> update(PetPedigreeModel obj) async {
    int result = await _pedigreeDao.update(obj);
    if (result > 0) {
      _pedigreeList.add(obj);
    }
    return result;
  }

  Future<List<PetPedigreeModel>> getAllRows() async {
    List<Map<String, dynamic>> maps =  await _pedigreeDao.queryAllRows();
    _pedigreeList.clear();
    maps.forEach((map) => _pedigreeList.add(PetPedigreeModel.fromMap(map)));
    return _pedigreeList;
  }

  Future<List<PetPedigreeModel>> getRows({int limit = 10, int offset = 0}) async {
    List<Map<String, dynamic>> maps =  await _pedigreeDao.queryRows(limit, offset);
    // print(maps);
    _pedigreeList.clear();
    maps.forEach((map) => _pedigreeList.add(PetPedigreeModel.fromMap(map)));
    return _pedigreeList;
  }

  // Retorna a linha que contem o id do model passado. Caso nao encontre
  // retorna null
  Future<PetPedigreeModel> getById(int id) async {
    return _pedigreeDao.queryById(PetPedigreeModel(idPetPedigree: id)).then((map) => PetPedigreeModel.fromMap(map));
  }

  Future<int> delete(int id) async {
    int result = await _pedigreeDao.delete(PetPedigreeModel(idPetPedigree: id));
    _pedigreeList.removeWhere((item) => item.idPetPedigree == id);
    return result;
  }
}