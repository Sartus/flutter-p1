

import 'package:flutter/material.dart';
import 'package:pet/common/commmon.dart';
import 'package:pet/dao/pet_healt_tp_dao.dart';
import 'package:pet/model/pet_healt_tp_model.dart';

class PetHealthTpController {
  final PetHealthTpDao _healthDao;
  final List<PetHealthTpModel> _healthList  = [];
  // final Widget _healthGui;
  // final EnumSideType _type;

  PetHealthTpController({EnumSideType type = EnumSideType.client, Widget gui}) :
    // _type = type,
    _healthDao = PetHealthTpDao(type: type);
    // _healthGui = gui;

  Future<List<PetHealthTpModel>> getAddressList () async { 
    return _healthList.isEmpty? getAllRows() : _healthList;
  }

  Future<int> insert(PetHealthTpModel obj) async {
    // result é o conteudo da PK da linha inserida.
    int result;
    try {
      result = await _healthDao.insert(obj);
    } catch(e) {
      return 0;
    }
    _healthList.add(obj);
    return result;
  }

  Future<int> update(PetHealthTpModel obj) async {
    int result = await _healthDao.update(obj);
    if (result > 0) {
      _healthList.add(obj);
    }
    return result;
  }

  Future<List<PetHealthTpModel>> getAllRows() async {
    List<Map<String, dynamic>> maps =  await _healthDao.queryAllRows();
    _healthList.clear();
    maps.forEach((map) => _healthList.add(PetHealthTpModel.fromMap(map)));
    return _healthList;
  }

  Future<List<PetHealthTpModel>> getRows({int limit = 10, int offset = 0}) async {
    List<Map<String, dynamic>> maps =  await _healthDao.queryRows(limit, offset);
    // print(maps);
    _healthList.clear();
    maps.forEach((map) => _healthList.add(PetHealthTpModel.fromMap(map)));
    return _healthList;
  }

  // Retorna a linha que contem o id do model passado. Caso nao encontre
  // retorna null
  Future<PetHealthTpModel> getById(int id) async {
    return _healthDao.queryById(PetHealthTpModel(idPetHealthTp: id)).then((map) => PetHealthTpModel.fromMap(map));
  }

  Future<int> delete(int id) async {
    // return _healthDao.delete(pet);
    int result = await _healthDao.delete(PetHealthTpModel(idPetHealthTp: id));
    _healthList.removeWhere((item) => item.idPetHealthTp == id);
    return result;
  }
}