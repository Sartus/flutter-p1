

import 'package:flutter/material.dart';
import 'package:pet/common/commmon.dart';
import 'package:pet/dao/sc_user_scheduler_dao.dart';
import 'package:pet/model/sc_user_scheduler_model.dart';

class UserSchedulerController {
  final UserScheduleDao _userSchedulerDao;
  final List<UserSchedulerModel> _userSchedulerList  = [];
  // final Widget _userSchedulerGui;
  // final EnumSideType _type;

  UserSchedulerController({EnumSideType type = EnumSideType.client, Widget gui}) :
    // _type = type,
    _userSchedulerDao = UserScheduleDao(type: type);
    // _userSchedulerGui = gui;
  
  Future<List<UserSchedulerModel>> getAddressList () async { 
    return _userSchedulerList.isEmpty? getAllRows() : _userSchedulerList;
  }

  Future<int> insert(UserSchedulerModel obj) async {
    // result é o conteudo da PK da linha inserida.
    int result;
    try {
      result = await _userSchedulerDao.insert(obj);
    } catch(e) {
      return 0;
    }
    _userSchedulerList.add(obj);
    return result;
  }

  Future<int> update(UserSchedulerModel obj) async {
    int result = await _userSchedulerDao.update(obj);
    if (result > 0) {
      _userSchedulerList.add(obj);
    }
    return result;
  }

  Future<List<UserSchedulerModel>> getAllRows() async {
    List<Map<String, dynamic>> maps =  await _userSchedulerDao.queryAllRows();
    _userSchedulerList.clear();
    maps.forEach((map) => _userSchedulerList.add(UserSchedulerModel.fromMap(map)));
    return _userSchedulerList;
  }

  Future<List<UserSchedulerModel>> getRows({int limit = 10, int offset = 0}) async {
    List<Map<String, dynamic>> maps =  await _userSchedulerDao.queryRows(limit, offset);
    // print(maps);
    _userSchedulerList.clear();
    maps.forEach((map) => _userSchedulerList.add(UserSchedulerModel.fromMap(map)));
    return _userSchedulerList;
  }

  // Retorna a linha que contem o id do model passado. Caso nao encontre
  // retorna null
  Future<UserSchedulerModel> getById(int id) async {
    return _userSchedulerDao.queryById(UserSchedulerModel(idUserSchedule: id)).then((map) => UserSchedulerModel.fromMap(map));
  }

  Future<int> delete(int id) async {
    int result = await _userSchedulerDao.delete(UserSchedulerModel(idUserSchedule: id));
    _userSchedulerList.removeWhere((item) => item.idUserSchedule == id);
    return result;
  }
}