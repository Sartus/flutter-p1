

import 'package:flutter/material.dart';
import 'package:pet/common/commmon.dart';
import 'package:pet/dao/pet_pet_dao.dart';
import 'package:pet/model/pet_pet_model.dart';

class PetController {
  final PetDao _petDao;
  final List<PetModel> _petList  = [];
  // final Widget _petGui;
  // final EnumSideType _type;

  PetController({EnumSideType type = EnumSideType.client, Widget gui}) :
    // _type = type,
    _petDao = PetDao(type: type);
    // _petGui = gui;
  
  Future<List<PetModel>> getAddressList () async { 
    return _petList.isEmpty? getAllRows() : _petList;
  }

  Future<int> insert(PetModel obj) async {
    int result;
    try {
      result = await _petDao.insert(obj);
    } catch(e) {
      return 0;
    }
    _petList.add(obj);
    return result;
  }

  Future<int> update(PetModel obj) async {
    int result = await _petDao.update(obj);
    if (result > 0) {
      _petList.add(obj);
    }
    return result;
  }

  Future<List<PetModel>> getAllRows() async {
    List<Map<String, dynamic>> maps =  await _petDao.queryAllRows();
    _petList.clear();
    maps.forEach((map) => _petList.add(PetModel.fromMap(map)));
    return _petList;
  }

  Future<List<PetModel>> getRows({int limit = 10, int offset = 0}) async {
    List<Map<String, dynamic>> maps =  await _petDao.queryRows(limit, offset);
    // print(maps);
    _petList.clear();
    maps.forEach((map) => _petList.add(PetModel.fromMap(map)));
    return _petList;
  }

  // Retorna a linha que contem o id do model passado. Caso nao encontre
  // retorna null
  Future<PetModel> getById(int id) async {
    return _petDao.queryById(PetModel(idPet: id)).then((map) => PetModel.fromMap(map));
  }

  // Retorna a linha que contem o id do model passado. Caso nao encontre
  // retorna null
  Future<PetModel> getByUserId(int id) async {
    return _petDao.queryByUserId(PetModel(idPet: id)).then((map) => PetModel.fromMap(map));
  }

  Future<int> delete(int id) async {
    int result = await _petDao.delete(PetModel(idPet: id));
    _petList.removeWhere((item) => item.idPet == id);
    return result;
  }
}