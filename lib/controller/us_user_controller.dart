

import 'package:flutter/material.dart';
import 'package:pet/common/commmon.dart';
import 'package:pet/dao/us_user_dao.dart';
import 'package:pet/model/us_user_model.dart';

class UserController {
  final UserDao _userDao;
  final List<UserModel> _userList  = [];
  // final Widget _userGui;
  // final EnumSideType _type;

  UserController({EnumSideType type = EnumSideType.client, Widget gui}) :
    // _type = type,
    _userDao = UserDao(type: type);
    // _userGui = gui;
  
  Future<List<UserModel>> getAddressList () async { 
    return _userList.isEmpty? getAllRows() : _userList;
  }

  Future<int> insert(UserModel obj) async {
    // result é o conteudo da PK da linha inserida.
    int result;
    try {
      result = await _userDao.insert(obj);
    } catch(e) {
      return 0;
    }
    _userList.add(obj);
    return result;
  }

  Future<int> update(UserModel obj) async {
    int result = await _userDao.update(obj);
    if (result > 0) {
      _userList.add(obj);
    }
    return result;
  }

  Future<List<UserModel>> getAllRows() async {
    List<Map<String, dynamic>> maps =  await _userDao.queryAllRows();
    _userList.clear();
    maps.forEach((map) => _userList.add(UserModel.fromMap(map)));
    return _userList;
  }

  Future<List<UserModel>> getRows({int limit = 10, int offset = 0}) async {
    List<Map<String, dynamic>> maps =  await _userDao.queryRows(limit, offset);
    // print(maps);
    _userList.clear();
    maps.forEach((map) => _userList.add(UserModel.fromMap(map)));
    return _userList;
  }

  // Retorna a linha que contem o id do model passado. Caso nao encontre
  // retorna null
  Future<UserModel> getById(int id) async {
    return _userDao.queryById(UserModel(idUser: id)).then((map) => UserModel.fromMap(map));
  }

  Future<UserModel> getByName(String name) async {
    // _userList.clear();
    // _userDao.queryByName(UserModel(userName: name)).then(
    //   (maps) => maps.forEach((map) => _userList.add(UserModel.fromMap(map))));
    return _userDao.queryByName(UserModel(userName: name)).then((map) => UserModel.fromMap(map));
  }
  
  Future<int> delete(int id) async {
    int result = await _userDao.delete(UserModel(idUser: id));
    _userList.removeWhere((item) => item.idUser == id);
    return result;
  }

}