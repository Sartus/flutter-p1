

import 'package:flutter/material.dart';
import 'package:pet/common/commmon.dart';
import 'package:pet/dao/pet_size_tp_dao.dart';
import 'package:pet/model/pet_size_tp_model.dart';

class PetSizeTpController {
  final PetSizeTpDao _sizeDao;
  final List<PetSizeTpModel> _sizeList  = [];
  // final Widget _sizeGui;
  // final EnumSideType _type;

  PetSizeTpController({EnumSideType type = EnumSideType.client, Widget gui}) :
    // _type = type,
    _sizeDao = PetSizeTpDao(type: type);
    // _sizeGui = gui;
  
  Future<List<PetSizeTpModel>> getAddressList () async { 
    return _sizeList.isEmpty? getAllRows() : _sizeList;
  }

  Future<int> insert(PetSizeTpModel obj) async {
    // result é o conteudo da PK da linha inserida.
    int result;
    try {
      result = await _sizeDao.insert(obj);
    } catch(e) {
      return 0;
    }
    _sizeList.add(obj);
    return result;
  }

  Future<int> update(PetSizeTpModel obj) async {
    int result = await _sizeDao.update(obj);
    if (result > 0) {
      _sizeList.add(obj);
    }
    return result;
  }


  Future<List<PetSizeTpModel>> getAllRows() async {
    List<Map<String, dynamic>> maps =  await _sizeDao.queryAllRows();
    _sizeList.clear();
    maps.forEach((map) => _sizeList.add(PetSizeTpModel.fromMap(map)));
    return _sizeList;
  }

  Future<List<PetSizeTpModel>> getRows({int limit = 10, int offset = 0}) async {
    List<Map<String, dynamic>> maps =  await _sizeDao.queryRows(limit, offset);
    // print(maps);
    _sizeList.clear();
    maps.forEach((map) => _sizeList.add(PetSizeTpModel.fromMap(map)));
    return _sizeList;
  }

  // Retorna a linha que contem o id do model passado. Caso nao encontre
  // retorna null
  Future<PetSizeTpModel> getById(int id) async {
    return _sizeDao.queryById(PetSizeTpModel(idPetSizeTp: id)).then((map) => PetSizeTpModel.fromMap(map));
  }

  Future<int> delete(int id) async {
    int result = await _sizeDao.delete(PetSizeTpModel(idPetSizeTp: id));
    _sizeList.removeWhere((item) => item.idPetSizeTp == id);
    return result;
  }
}