

import 'package:pet/common/commmon.dart';
import 'package:pet/dao/base_dao.dart';
import 'package:pet/db/client_conn_db.dart';
import 'package:pet/db/server_conn_db.dart';

class PetDao extends BaseDao{
  static const String _nameTable = "`PET_Pet`";

  PetDao({EnumSideType type = EnumSideType.client}) : super(
    (type == EnumSideType.client) ? 
    ClientDatabase.instance.database :
    ServerDatabase.instance.database,
    _nameTable);

  Future<Map<String, dynamic>> queryByUserId(var model) async {
    int id = model.userId;
    String raw = "SELECT * \n" + 
    "FROM US_User \n" +
    "WHERE US_User.IdUser = $id \n" +
    "AND US_User.IdUser IN ( \n" +
    "SELECT PET_PetOwner.UserId \n" +
    "from  PET_PetOwner,PET_PetVsPetOwner \n" +
    "WHERE PET_PetOwner.IdPetOwner = PET_PetVsPetOwner.PetOwnerId \n" +
    ");";
    return await super.rawQuery(raw);
  }
}