
import 'package:sqflite/sqflite.dart';

class BaseDao {
  final _database;
  final String _nameTable;


  BaseDao(var database, String nameTable) :
    _database = database,
    _nameTable = nameTable;

  get database => _database;
  get nameTable => _nameTable;

  // Insert/Update uma linha na tabela onde cada chave 
  // no Map é um nome de coluna e o valor é o valor da coluna. 
  // O valor de retorno é o id da linha inserida.
  // Future<int> upsert(var model) async {
  //   Map<String, dynamic> map = model.toMap();
  //   Database db = await _database;
  //   return await db.insert(_nameTable, map, nullColumnHack: map.keys.first, conflictAlgorithm: ConflictAlgorithm.replace);
  // }

  // Insert uma linha na tabela onde cada chave 
  // no Map é um nome de coluna e o valor é o valor da coluna. 
  // O valor de retorno é o id da linha inserida.
  Future <int> insert(var model) async {
    Map<String, dynamic> map = model.toMap();
    Database db = await _database;
    return await db.insert(_nameTable, map, nullColumnHack: map.keys.first);
  }

  // Insert uma linha na tabela onde cada chave 
  // no Map é um nome de coluna e o valor é o valor da coluna. 
  // O valor de retorno é o id da linha inserida.
  Future <int> update(var model) async {
    Map<String, dynamic> map = model.toMap();
    String idColumnName = map.keys.first;
    int id = map.values.first;
    Database db = await _database;
    return await db.update(_nameTable, map, 
    where: "$idColumnName = ?",
    whereArgs: [id]);
  }

  // Retorna todas as linhas da tabela como umma lista de mapas, onde cada
  // mapa é uma lista de valores de colunas.
  Future<List<Map<String, dynamic>>> queryAllRows() async {
    Database db = await _database;
    return await db.query(_nameTable);
  }

  // Retorna um numero de linhas, definida por limit, a partir do offset.
  Future<List<Map<String, dynamic>>> queryRows(int limit, int offset) async {
    Database db = await _database;
    return await db.query(_nameTable, limit: limit, offset: offset);
  }

  // Retorna a linha da tabela que contem o mesmo id que o do model passado
  Future<Map<String, dynamic>> queryById(var model) async {
    Database db = await _database;
    Map<String, dynamic> map = model.toMap();
    String idColumnName = map.keys.first;
    int id = map.values.first;
    final maps = await db.query(_nameTable,
     where: '$idColumnName = ?',
     whereArgs: [id]);
    if (maps.length > 0) {
      return maps.first;
    }
    return null;
  }

  // Exclui a linha especificada pelo id. O número de linhas afetadas é
  // retornada. Isso deve ser igual a 1, contanto que a linha exista.
  Future<int> delete(var model) async {
    Database db = await _database;
    Map<String, dynamic> map = model.toMap();
    String nameColumnId = map.keys.first;
    int id = map.values.first;
    return await db.delete(_nameTable, where: '$nameColumnId = ?', whereArgs: [id]);
  }

  // Retorna o resultado da query passada.
  Future<dynamic> rawQuery(String rawString) async {
    Database db = await _database;
    return await db.rawQuery(rawString);
  }
}