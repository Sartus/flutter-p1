

import 'package:pet/common/commmon.dart';
import 'package:pet/dao/base_dao.dart';
import 'package:pet/db/client_conn_db.dart';
import 'package:pet/db/server_conn_db.dart';
import 'package:pet/model/pet_vs_pet_walker_model.dart';

class PetOwnerDao extends BaseDao{
  static const String _nameTable = "`PET_PetOwner`";

  PetOwnerDao({EnumSideType type = EnumSideType.client}) : super(
    (type == EnumSideType.client) ? 
    ClientDatabase.instance.database :
    ServerDatabase.instance.database,
    _nameTable);

  
  @override
  get database => super.database;

  Future<dynamic> setWalker(var pet, var walker) async {
    int idPet = pet.idPet;
    int idWalker = walker.idPetWalker;
    String raw = "SELECT COUNT(*)" + 
    "FROM PET_PetVsPetWalker" +
    "WHERE PET_PetVsPetWalker.IdPet = $idPet" + 
    "AND PET_PetVsPetWalker.IdWalker = $idWalker;";

    List<Map<String, dynamic>> maps = await super.rawQuery(raw);
    
    if (maps.length == 0){
      return await super.insert(
        PetVsPetWalkerModel(petId: idPet,petWalkerId: idWalker)
      );
    }
    else {
      return null;
    }
  }

  Future<Map<String, dynamic>> queryByUserId(var model) async {
    int id = model.userId;
    String raw = "SELECT * \n" + 
    "FROM PET_PetOwner \n" +
    "WHERE PET_PetOwner.UserId = $id;";
    var result = await super.rawQuery(raw);
    return result.length == 0 ? null : result.first;
    // return await super.rawQuery(raw);
  }
}