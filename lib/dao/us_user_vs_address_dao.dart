

import 'package:pet/common/commmon.dart';
import 'package:pet/dao/base_dao.dart';
import 'package:pet/db/client_conn_db.dart';
import 'package:pet/db/server_conn_db.dart';

class UserVsAddressDao extends BaseDao{
  static const String _nameTable = "`US_UserVsAddress`";

  UserVsAddressDao({EnumSideType type = EnumSideType.client}) : super(
    (type == EnumSideType.client) ? 
    ClientDatabase.instance.database :
    ServerDatabase.instance.database,
    _nameTable);

  // @override
  // get _database => super.database;

  Future<List<Map<String, dynamic>>> matchMaker(var user) async {
    int id = user.idUser;
    String raw = "Select US_UserVsAddress.UserId \n" + 
    "From US_UserVsAddress, PET_PetWalker \n" +
    "Where  US_UserVsAddress.AddressId IN ( \n" +
    "Select US_UserVsAddress.AddressId \n" +
    "From US_UserVsAddress, US_User \n" + 
    "Where  US_User.IdUser = $id \n" + 
    "AND US_User.IdUser = US_UserVsAddress.UserId ) \n" +
    "AND PET_PetWalker.UserId = US_UserVsAddress.UserId;";
    
    List<Map<String, dynamic>> maps = await super.rawQuery(raw);
    
    if (maps.length > 0){
      return maps;
    }
    else {
      return null;
    }
  }
  
}