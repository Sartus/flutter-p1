

import 'dart:convert';

class PetOwnerModel {
  int idPetOwner;
  int userId;
  int petStatusTpId;
  PetOwnerModel({
    this.idPetOwner,
    this.userId,
    this.petStatusTpId,
  });

  PetOwnerModel copyWith({
    int idPetOwner,
    int userId,
    int petStatusTpId,
  }) {
    return PetOwnerModel(
      idPetOwner: idPetOwner ?? this.idPetOwner,
      userId: userId ?? this.userId,
      petStatusTpId: petStatusTpId ?? this.petStatusTpId,
    );
  }

  Map<String, dynamic> toMap() {
    return {
      'idPetOwner': idPetOwner,
      'userId': userId,
      'petStatusTpId': petStatusTpId,
    };
  }

  factory PetOwnerModel.fromMap(Map<String, dynamic> map) {
    if (map == null) return null;
  
    return PetOwnerModel(
      idPetOwner: map['IdPetOwner'],
      userId: map['UserId'],
      petStatusTpId: map['PetStatusTpId'],
    );
  }

  String toJson() => json.encode(toMap());

  factory PetOwnerModel.fromJson(String source) => PetOwnerModel.fromMap(json.decode(source));

  @override
  String toString() => 'PetOwner(idPetOwner: $idPetOwner, userId: $userId, petStatusTpId: $petStatusTpId)';

  @override
  bool operator ==(Object o) {
    if (identical(this, o)) return true;
  
    return o is PetOwnerModel &&
      o.idPetOwner == idPetOwner &&
      o.userId == userId &&
      o.petStatusTpId == petStatusTpId;
  }

  @override
  int get hashCode => idPetOwner.hashCode ^ userId.hashCode ^ petStatusTpId.hashCode;
}
