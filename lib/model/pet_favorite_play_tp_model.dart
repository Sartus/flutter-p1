import 'dart:convert';



class PetFavoritePlayTpModel {
  int idPetFavoritePlayTp;
  String petFavoritePlayTpCd;
  String petFavoritePlayTpAc;
  String petFavoritePlayTpDs;
  PetFavoritePlayTpModel({
    this.idPetFavoritePlayTp,
    this.petFavoritePlayTpCd,
    this.petFavoritePlayTpAc,
    this.petFavoritePlayTpDs,
  });

  PetFavoritePlayTpModel copyWith({
    int idPetFavoritePlayTp,
    String petFavoritePlayTpCd,
    String petFavoritePlayTpAc,
    String petFavoritePlayTpDs,
  }) {
    return PetFavoritePlayTpModel(
      idPetFavoritePlayTp: idPetFavoritePlayTp ?? this.idPetFavoritePlayTp,
      petFavoritePlayTpCd: petFavoritePlayTpCd ?? this.petFavoritePlayTpCd,
      petFavoritePlayTpAc: petFavoritePlayTpAc ?? this.petFavoritePlayTpAc,
      petFavoritePlayTpDs: petFavoritePlayTpDs ?? this.petFavoritePlayTpDs,
    );
  }

  Map<String, dynamic> toMap() {
    return {
      'idPetFavoritePlayTp': idPetFavoritePlayTp,
      'petFavoritePlayTpCd': petFavoritePlayTpCd,
      'petFavoritePlayTpAc': petFavoritePlayTpAc,
      'petFavoritePlayTpDs': petFavoritePlayTpDs,
    };
  }

  factory PetFavoritePlayTpModel.fromMap(Map<String, dynamic> map) {
    if (map == null) return null;
  
    return PetFavoritePlayTpModel(
      idPetFavoritePlayTp: map['IdPetFavoritePlayTp'],
      petFavoritePlayTpCd: map['PetFavoritePlayTpCd'],
      petFavoritePlayTpAc: map['PetFavoritePlayTpAc'],
      petFavoritePlayTpDs: map['PetFavoritePlayTpDs'],
    );
  }

  String toJson() => json.encode(toMap());

  factory PetFavoritePlayTpModel.fromJson(String source) => PetFavoritePlayTpModel.fromMap(json.decode(source));

  @override
  String toString() {
    return 'PetFavoritePlayTp(idPetFavoritePlayTp: $idPetFavoritePlayTp, petFavoritePlayTpCd: $petFavoritePlayTpCd, petFavoritePlayTpAc: $petFavoritePlayTpAc, petFavoritePlayTpDs: $petFavoritePlayTpDs)';
  }

  @override
  bool operator ==(Object o) {
    if (identical(this, o)) return true;
  
    return o is PetFavoritePlayTpModel &&
      o.idPetFavoritePlayTp == idPetFavoritePlayTp &&
      o.petFavoritePlayTpCd == petFavoritePlayTpCd &&
      o.petFavoritePlayTpAc == petFavoritePlayTpAc &&
      o.petFavoritePlayTpDs == petFavoritePlayTpDs;
  }

  @override
  int get hashCode {
    return idPetFavoritePlayTp.hashCode ^
      petFavoritePlayTpCd.hashCode ^
      petFavoritePlayTpAc.hashCode ^
      petFavoritePlayTpDs.hashCode;
  }
}
