import 'dart:convert';


class PetSpecialCareTpModel {
  int idPetSpecialCareTp;
  String petSpecialCareTpCd;
  String petSpecialCareTpAc;
  String petSpecialCareTpDs;
  PetSpecialCareTpModel({
    this.idPetSpecialCareTp,
    this.petSpecialCareTpCd,
    this.petSpecialCareTpAc,
    this.petSpecialCareTpDs,
  });

  PetSpecialCareTpModel copyWith({
    int idPetSpecialCareTp,
    String petSpecialCareTpCd,
    String petSpecialCareTpAc,
    String petSpecialCareTpDs,
  }) {
    return PetSpecialCareTpModel(
      idPetSpecialCareTp: idPetSpecialCareTp ?? this.idPetSpecialCareTp,
      petSpecialCareTpCd: petSpecialCareTpCd ?? this.petSpecialCareTpCd,
      petSpecialCareTpAc: petSpecialCareTpAc ?? this.petSpecialCareTpAc,
      petSpecialCareTpDs: petSpecialCareTpDs ?? this.petSpecialCareTpDs,
    );
  }

  Map<String, dynamic> toMap() {
    return {
      'idPetSpecialCareTp': idPetSpecialCareTp,
      'petSpecialCareTpCd': petSpecialCareTpCd,
      'petSpecialCareTpAc': petSpecialCareTpAc,
      'petSpecialCareTpDs': petSpecialCareTpDs,
    };
  }

  factory PetSpecialCareTpModel.fromMap(Map<String, dynamic> map) {
    if (map == null) return null;
  
    return PetSpecialCareTpModel(
      idPetSpecialCareTp: map['IdPetSpecialCareTp'],
      petSpecialCareTpCd: map['PetSpecialCareTpCd'],
      petSpecialCareTpAc: map['PetSpecialCareTpAc'],
      petSpecialCareTpDs: map['PetSpecialCareTpDs'],
    );
  }

  String toJson() => json.encode(toMap());

  factory PetSpecialCareTpModel.fromJson(String source) => PetSpecialCareTpModel.fromMap(json.decode(source));

  @override
  String toString() {
    return 'PetSpecialCareTp(idPetSpecialCareTp: $idPetSpecialCareTp, petSpecialCareTpCd: $petSpecialCareTpCd, petSpecialCareTpAc: $petSpecialCareTpAc, petSpecialCareTpDs: $petSpecialCareTpDs)';
  }

  @override
  bool operator ==(Object o) {
    if (identical(this, o)) return true;
  
    return o is PetSpecialCareTpModel &&
      o.idPetSpecialCareTp == idPetSpecialCareTp &&
      o.petSpecialCareTpCd == petSpecialCareTpCd &&
      o.petSpecialCareTpAc == petSpecialCareTpAc &&
      o.petSpecialCareTpDs == petSpecialCareTpDs;
  }

  @override
  int get hashCode {
    return idPetSpecialCareTp.hashCode ^
      petSpecialCareTpCd.hashCode ^
      petSpecialCareTpAc.hashCode ^
      petSpecialCareTpDs.hashCode;
  }
}
