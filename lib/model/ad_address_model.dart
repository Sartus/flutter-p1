import 'dart:convert';

class AddressModel {
  int idAddress;
  String neighborhood;
  String city;
  String state;
  String address;
  String zipCode;
  double latitude;
  double longitude;
  AddressModel({
    this.idAddress,
    this.neighborhood,
    this.city,
    this.state,
    this.address,
    this.zipCode,
    this.latitude,
    this.longitude,
  });


  AddressModel copyWith({
    int idAddress,
    String neighborhood,
    String city,
    String state,
    String address,
    String zipCode,
    double latitude,
    double longitude,
  }) {
    return AddressModel(
      idAddress: idAddress ?? this.idAddress,
      neighborhood: neighborhood ?? this.neighborhood,
      city: city ?? this.city,
      state: state ?? this.state,
      address: address ?? this.address,
      zipCode: zipCode ?? this.zipCode,
      latitude: latitude ?? this.latitude,
      longitude: longitude ?? this.longitude,
    );
  }

  Map<String, dynamic> toMap() {
    return {
      'idAddress': idAddress,
      'neighborhood': neighborhood,
      'city': city,
      'state': state,
      'address': address,
      'zipCode': zipCode,
      'latitude': latitude,
      'longitude': longitude,
    };
  }

  factory AddressModel.fromMap(Map<String, dynamic> map) {
    if (map == null) return null;
  
    return AddressModel(
      idAddress: map['IdAddress'],
      neighborhood: map['Neighborhood'],
      city: map['City'],
      state: map['State'],
      address: map['Address'],
      zipCode: map['ZipCode'],
      latitude: map['Latitude'],
      longitude: map['Longitude'],
    );
  }

  String toJson() => json.encode(toMap());

  factory AddressModel.fromJson(String source) => AddressModel.fromMap(json.decode(source));

  @override
  String toString() {
    return 'Address(idAddress: $idAddress, neighborhood: $neighborhood, city: $city, state: $state, address: $address, zipCode: $zipCode, latitude: $latitude, longitude: $longitude)';
  }

  @override
  bool operator ==(Object o) {
    if (identical(this, o)) return true;
  
    return o is AddressModel &&
      o.idAddress == idAddress &&
      o.neighborhood == neighborhood &&
      o.city == city &&
      o.state == state &&
      o.address == address &&
      o.zipCode == zipCode &&
      o.latitude == latitude &&
      o.longitude == longitude;
  }

  @override
  int get hashCode {
    return idAddress.hashCode ^
      neighborhood.hashCode ^
      city.hashCode ^
      state.hashCode ^
      address.hashCode ^
      zipCode.hashCode ^
      latitude.hashCode ^
      longitude.hashCode;
  }
}
