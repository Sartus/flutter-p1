

import 'package:pet/common/commmon.dart';
import 'package:pet/controller/ad_address_controller.dart';
import 'package:pet/controller/pet_owner_controller.dart';
import 'package:pet/controller/pet_pet_controller.dart';
import 'package:pet/controller/pet_pet_vs_pet_owner_controller.dart';
import 'package:pet/controller/pet_picture_controller.dart';
import 'package:pet/controller/pet_vs_pet_walker_controller.dart';
import 'package:pet/controller/pet_walker_controller.dart';
import 'package:pet/controller/us_user_controller.dart';
import 'package:pet/controller/us_user_vs_address_controller.dart';
import 'package:pet/model/ad_address_model.dart';
import 'package:pet/model/pet_owner_model.dart';
import 'package:pet/model/pet_pet_model.dart';
import 'package:pet/model/pet_vs_pet_owner_model.dart';
import 'package:pet/model/pet_walker_model.dart';
import 'package:pet/model/us_user_model.dart';
import 'package:pet/model/us_user_vs_address_model.dart';

class MasterService {
  UserModel _activeUser;
  PetWalkerModel _activeWalker;
  PetOwnerModel _activeOwner;
  AddressModel address;
  final List<PetWalkerModel> _walkerList = [];
  final List<PetModel> _petList = [];
  // final List<String> _pictureList = [];
  final EnumSideType _type;

  // torna esta classe singleton
  MasterService._privateConstructor({EnumSideType type = EnumSideType.server}) :
  _type = type;
  static final MasterService instance = MasterService._privateConstructor();

  // MasterService({EnumSideType type = EnumSideType.server}) :
  //   _type = type;

  Future<int> _cadastraPet(PetModel model) {
    PetController crtl = PetController(type: EnumSideType.server);
    return crtl.insert(model);
  }

  // Faz insert na tabela PetVsPetOwnerModel
  Future<int> _cadastraPetVsPetOwner(PetVsPetOwnerModel model) {
    PetVsPetOwnerController crtl = PetVsPetOwnerController(type: EnumSideType.server);
    return crtl.insert(model);
  }

  // Faz insert na tabela UserModel
  Future<int> _cadastraUser(UserModel model) {
    UserController crtl = UserController(type: _type);
    return crtl.insert(model);
  }

  // Faz insert na tabela PetOwnerModel
  Future<int> _cadastraPetOwner(PetOwnerModel model) {
    PetOwnerController crtl = PetOwnerController(type: EnumSideType.server);
    return crtl.insert(model);
  }

  // Faz insert na tabela PetWalkerModel
  Future<int> _cadastraPetWalker(PetWalkerModel model) {
    PetWalkerController crtl = PetWalkerController(type: EnumSideType.server);
    return crtl.insert(model);
  }

  // Faz insert na tabela AddressModel
  Future<int> _cadastraAddress(AddressModel model) {
    AddressController crtl = AddressController(type: EnumSideType.server);
    return crtl.insert(model);
  }

  // Faz insert na tabela UserVsAddressModel
  Future<int> _cadastraUserVsAddress(UserVsAddressModel model) {
    UserVsAddressController crtl = UserVsAddressController(type: EnumSideType.server);
    return crtl.insert(model);
  }

  // Devolve o usuario ativo
  Future<UserModel> get activeUser async => _activeUser;

  // Devolve o PetOwnerModel pelo idUser passado.
  Future<PetOwnerModel> getOwnerByUserId(int idUser) async {
    PetOwnerController ctrl = PetOwnerController(type: EnumSideType.server);
    return await ctrl.getByUserId(idUser);
  }

  // Devolve o PetWalkerModel pelo idUser passado.
  Future<PetWalkerModel> getWalkerByUserId(int idUser) async {
    PetWalkerController ctrl = PetWalkerController(type: EnumSideType.server);
    return await ctrl.getByUserId(idUser);
  }

  // Devolve o PetModel pelo idUser passado.
  Future<PetModel> getPetByUserId(int idUser) async {
    PetController ctrl = PetController(type: EnumSideType.server);
    return await ctrl.getByUserId(idUser);
  }

  // Devolve o AddressModel pelo idUser passado.
  Future<AddressModel> getAddressByUserId(int idUser) async {
    AddressController ctrl = AddressController(type: EnumSideType.server);
    return await ctrl.getByUserId(idUser);
  }

  // Devolve o nome do usuario e o endereco pelo WalkerId passado.
  Future<Map<String, dynamic>> getByWalkerId(int id) async {
    PetWalkerController ctrl = PetWalkerController(type: EnumSideType.server);
    return await ctrl.getByPetWalkerId(id);
  }

  // Devolve o nome do pet pelo WalkerId passado.
  Future<List<Map<String, dynamic>>> getByPetByWalkerId(int id) async {
    PetWalkerController ctrl = PetWalkerController(type: EnumSideType.server);
    return await ctrl.getPetByWalkerId(id);
  }

  // Faz a validacao e login do usuario
  Future<bool> doLogin(UserModel model) async {
    UserController crtl = UserController(type: _type);
    UserModel map = await crtl.getByName(model.userName);
    print(map);
    if(map == null) {
      return false;
    } else if (model.userPwd == map.userPwd) {
      _activeUser = map;
      return true;
    }
    return false;
  }

  // Faz o registro do usuario.
  Future<bool> doRegisterUser(UserModel model, {bool isOwner, bool isWalker}) async {
    UserController crtl = UserController(type: _type);
    UserModel map = await crtl.getByName(model.userName);
    if(map == null) { // User nao existe
      UserModel modelUser = UserModel(userName: model.userName, userPwd: model.userPwd);
      modelUser.idUser = await _cadastraUser(modelUser);
      _activeUser = modelUser;
      if (isOwner) {
        _activeOwner = PetOwnerModel(userId: modelUser.idUser);
        _activeOwner.idPetOwner = await _cadastraPetOwner(_activeOwner);
      }
      if(isWalker) {
        _activeWalker = PetWalkerModel(userId: modelUser.idUser);
        _activeWalker.idPetWalker = await _cadastraPetWalker(_activeWalker);
      }
      return true;
    }
    return false;
  }

  // Faz o cadastro do endereco do usuario
  Future<bool> doRegisterAddress(AddressModel model, {String number, String complementary}) async {
    int idAddress = await _cadastraAddress(model);
    if(idAddress > 0) {
      await _cadastraUserVsAddress(UserVsAddressModel(userId: _activeUser.idUser, addressId: idAddress));
      address = await AddressController().getById(idAddress);
      return true;
    }
    return false;
  }

  // Faz o matchmaker pelo idUser
  Future<List<PetWalkerModel>> getMatchMakerByUserId(int idUser) async {
    UserVsAddressController crtl = UserVsAddressController(type: EnumSideType.server);
    List<Map<String, dynamic>> maps =  await crtl.matchMaker(UserModel(idUser: idUser));
    maps.forEach((element) => _walkerList.add(PetWalkerModel.fromMap(element)));
    return _walkerList;
  }

  // Faz o cadastro do pet
  Future<bool> doRegisterPet(PetModel model) async {
    int id = await _cadastraPet(model);
    if(id > 0){
      model.idPet = id;
      PetOwnerController ctrl = PetOwnerController(type: _type);
      if(_activeOwner == null) {
        PetOwnerModel own = await ctrl.getByUserId(_activeUser.idUser);
        if(own == null) {
          own = PetOwnerModel(userId: _activeUser.idUser);
          id = await _cadastraPetOwner(own);
          own.idPetOwner = id;
        }
        _activeOwner = own;
      }
      _petList.add(model);
      id = await _cadastraPetVsPetOwner(
        PetVsPetOwnerModel(petId: id, petOwnerId: _activeOwner.idPetOwner));
      return true;
    }
    return false;
  }

  // Devolve o caminho relativo da imagem.
  Future<String> getPhoto() async {
    PetPictureController ctrl = PetPictureController(type: EnumSideType.server);
    String image;
    await ctrl.getImage().then((value) => image = value);
    return image;
  }

  // Devolve todos os walkers cadastrados
  Future<List<PetWalkerModel>> getWalkers() async {
    PetWalkerController ctrl = PetWalkerController(type: EnumSideType.server);
    return await ctrl.getWalkerList();
  }

  // Devolve o UserModel pelo userId passado
  Future<UserModel> getUserByUserId(int id) async {
    UserController ctrl = UserController(type: EnumSideType.server);
    return await ctrl.getById(id);
  }

  // Devolve o nome do usuario de acordo userId passado
  Future<String> getUserNameByUserId(int id) async {
    UserController ctrl = UserController(type: EnumSideType.server);
    String name;
    await ctrl.getById(id).then((value) => name = value.userName);
    return name;
  }

  // Devolve todos os PetWalkerId distintos que estao com pets cadastrados
  Future<List<int>> getDistinctPetWalkerId() async {
    final PetVsPetWalkerController  ctrl = PetVsPetWalkerController(type: EnumSideType.server);
    return await ctrl.getDistinctPetWalkerId();
  }

  // Devolve o PetWalkerModel referente ao userId passado.
  Future<PetWalkerModel> getWalkerById(int id) async {
    final PetWalkerController  ctrl = PetWalkerController(type: EnumSideType.server);
    return await ctrl.getById(id);
  }
}