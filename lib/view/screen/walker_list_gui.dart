
//https://medium.com/@dltlabs/how-to-build-a-flutter-card-list-in-less-than-10-minutes-9839f79a6c08
//https://simpleactivity435203168.wordpress.com/2019/09/19/to-do-list-in-flutter-with-sqlite-as-local-database/
//https://www.linkedin.com/pulse/to-do-list-flutter-sqlite-local-database-udara-abeythilake
//https://www.c-sharpcorner.com/article/how-to-create-listview-in-flutter-dynamic/
//https://www.codegrepper.com/code-examples/dart/how+to+make+a+database+for+flutter
//https://kodestat.gitbook.io/flutter/37-flutter-using-cards
//https://pusher.com/tutorials/flutter-listviews
//https://flutteragency.com/listtile-widget/
//https://stackoverflow.com/questions/52583856/make-buttons-in-a-row-have-the-same-width-in-flutter
//https://mrflutter.com/sizing-widgets-relative-to-parent-screen-size-in-flutter/
//https://medium.com/weekly-webtips/flutter-gesturedetector-86fc937aaf17

import 'package:flutter/material.dart';
import 'package:pet/view/screen/walker_detail_gui.dart';
//import 'package:pet/view/screen/login_gui.dart';
import 'package:pet/view/screen/user_address_gui.dart';
import 'package:pet/view/screen/user_gui.dart';
import 'package:pet/view/screen/pet_gui.dart';
//import 'package:pet/view/screen/walker_list_gui.dart';

class Walker {
  int id;
  String sName;
  String sAddress;
  int iRate;
  bool bFavorite;
  Walker(this.id,this.sName, this.sAddress, this.iRate, this.bFavorite);
}
class WalkerController {
  //List<Walker> oWalkerList = List<Walker>();
  List<Walker> oWalkerList = [];

  List<Walker> getWalkerList() {
    if (oWalkerList.isEmpty) {
      oWalkerList.add(Walker(1,'Flavio Antonio Carlos Sergio Marcos da Silva', 'Rua Marques de Sao Vicente Marques de Marques de Sao Vicente Marques Sao Vicente', 5,true));
      oWalkerList.add(Walker(2,'Sergio Sergio Marcos da Marcos Silva', 'Rua dos Oitis', 4,true));
      oWalkerList.add(Walker(3,'Maicon Sergio da Silva', 'Estr da Gavea', 3,false));
      oWalkerList.add(Walker(4,'Felipe Sergio da Silva', 'Rua dos Oitis', 2,false));
      oWalkerList.add(Walker(5,'Marcos Sergio da Silva', 'Rua Marques de Sao Vicente', 1,false));
      oWalkerList.add(Walker(6,'Joao Sergio da Silva', 'Estr da Gavea', 0,false));
      oWalkerList.add(Walker(7,'Mari Sergio da Silva', 'Rua Marques de Sao Vicente', 1,false));
      oWalkerList.add(Walker(8,'Jose Sergio da Silva', 'Rua dos Oitis', 2,true));
      oWalkerList.add(Walker(9,'Antonio Sergio da Silva', 'Estr da Gavea', 3,false));
      oWalkerList.add(Walker(10,'Carlos Sergio da Silva', 'Rua dos Oitis', 4,false));
    }

    return oWalkerList;
  }
}


class WalkerListGui extends StatefulWidget {
  @override
  State<StatefulWidget> createState() => new _WalkerListGuiState();
}

class _WalkerListGuiState extends State<WalkerListGui> {
  WalkerController oWalkerController = WalkerController();
  List<Walker> oWalkerList = [];
  final oColorList = [Colors.yellow, Colors.grey];
  double hInicio,hFim;
  //final oTitleList = ['bike', 'boat', 'bus', 'car','railway', 'run', 'subway', 'transit', 'walk'];
  //final oRateList =  [1     ,      2,     3,     4,        5,     4,        3,         2,      1];
  //final ControllerPetWalker oControllerPetWalker = ControllerPetWalker();
  //int iLenColorList=0;
  //int iLenTitleList =0;
  //int iLenWalkerList=0;

  @override
  Widget build(BuildContext context) {

    return Scaffold(
      
      appBar: _appBar(),
      body: _showListView(),
      bottomNavigationBar: _showBottomNavigationBar(),
    );
  }

  void removeItem(int index) {
    setState(() {
      oWalkerList.removeAt(index);
    });
  }

  void favorita(int index) {
    setState(() {
      oWalkerList[index].bFavorite = !oWalkerList[index].bFavorite;
    });
  }


  PreferredSizeWidget _appBar() {
    return AppBar(
      leading: new IconButton( 
        icon: new Icon(
          Icons.menu,
          //color: Colors.green[500],
        ),
        onPressed: () {
          //Show User Details (UserGui)
        },
      ), 

      title: Row(
        mainAxisAlignment: MainAxisAlignment.center,
        children: <Widget> [
          Expanded(
            flex: 9,
            //mainAxisAlignment: MainAxisAlignment.center,
            child: Row (
              children: <Widget> [
                Padding(
                  padding: const EdgeInsets.all(20.0),
                ),
                Icon(
                  Icons.pets,
                  //color: Colors.green[500],
                ),
                Padding(
                  padding: const EdgeInsets.all(10.0),
                ),
                Text (
                  'Pet Walking',
                ),
                //Padding(
                //  padding: const EdgeInsets.all(10.0),
                //),
              ],
            ),
          ),
          Expanded(
            flex:1,
            child: Icon(
              Icons.search,
              //color: Colors.green[500],
            ),
          ),
          //Padding(
          //  padding: const EdgeInsets.all(30.0),
          //),
        ],
      ),

    );
  }

  void _doUpdateListView() {
    this.oWalkerList = oWalkerController.getWalkerList();
    //Util.getI().showMsg(context, this.oWalkerList.length.toString());
    /*
    for (var name in this.oWalkerList) {
      print (name.sName);
      //dev.log(name.sName);
      //os_log('\n  We're using this');
      //debugPrint(name.sName);
    }
    */
  }

  Widget _showListView() {
    _doUpdateListView();
    
    return ListView.builder(
      itemCount: this.oWalkerList.length,
      //itemCount: oTitleList.length,
      itemBuilder: (context, iPos) {
        return _showCard(iPos);
      },
    );
  }

  Widget _showCard(int iPos) {
    Walker _oWalker=this.oWalkerList[iPos];
    String _sName=_oWalker.sName;
    //String _sName=oTitleList[iPos];
    String _sNameFirstLetterUpcase=_sName.substring(0,1).toUpperCase();
    //int _iRate=oRateList[iPos];
    int _iRate=_oWalker.iRate;
    int _iCurrentColor = iPos.isEven ? 0 : 1 ;

    /*
    List <Icon> oStarList = [];
    for (var oIcon in othis.oWalkerList) {
    for (var i=0 ; i < 5 ; i++) {
      if (_iRate > i) {
        oStarList.add(
          new Icon(Icons.star, size: 15.0, color: Colors.yellow)
        );
      } else {
        oStarList.add(
          new Icon(Icons.star_border, size: 15.0, color: Colors.grey)
        );
      }
    }
    */

    /*
    for (var i=0 ; i < 5 ; i++) {
      if (_iRate > i) {
        oStarList.add(
          new Icon(Icons.star, size: 15.0, color: Colors.yellow)
        );
      } else {
        oStarList.add(
          new Icon(Icons.star_border, size: 15.0, color: Colors.grey)
        );
      }
    }
    */

    Widget wAvatar = CircleAvatar (
      //isThreeLine: true,
      /*
      leading: Icon(Icons.save),
      leading: Container(
        //alignment: Alignment.center,
        //padding: const EdgeInsets.all(20.0),
        child: Image(
          height: 50,
          width: 50,
          //fit: BoxFit.contain,
          fit: BoxFit.cover,
          image: AssetImage('assets/images/dog011.jpg')
        ) 
      ),
      */
      backgroundColor: oColorList[_iCurrentColor],
      child: Text(
        _sNameFirstLetterUpcase,
        style: TextStyle(
          color: Colors.white,
          fontSize: 30,
          //fontWeight: FontWeight.bold
        ),
      ),
    );

    Widget wText = Expanded ( 
      flex: 6,
      child: Column (
        crossAxisAlignment: CrossAxisAlignment.stretch,
        children: <Widget> [
          Text(
            _sName,
            textAlign: TextAlign.left,
            maxLines: 1,
            overflow: TextOverflow.ellipsis,
            style: TextStyle(
              color: Colors.black,
              fontWeight: FontWeight.w600,
              fontSize: 20
            ),
          ),
          Text(
            //Address
            this.oWalkerList[iPos].sAddress,
            textAlign: TextAlign.left,
            maxLines: 1,
            overflow: TextOverflow.ellipsis,
            style: TextStyle(
              color: Colors.grey,
              fontWeight: FontWeight.w800,
              fontSize: 12
            ),
          ),               
        ],
      ),
    );

    /*
    Widget wIcon = Expanded ( 
      flex: 4,
      child: Row (
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget> [
                    new Icon(Icons.star, size: 15.0, color: Colors.yellow)
        );
      } else {
        oStarList.add(
          oStarList[0],
          oStarList[0],
          oStarList[0],
          oStarList[0],
          oStarList[0],
        ],
      ),
    ),
    */

    Widget wIcon = Expanded ( 
      flex: 4,
      child: Row (
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget> [
  //          Padding(
  //            padding: const EdgeInsets.all(5.0),
  //          ),
          Icon(
            _iRate >= 1 ? Icons.star : Icons.star_border,
            color: (_iRate >= 1 ? Colors.yellow : Colors.grey),
            size: 15,
          ),
          Icon(
            _iRate >= 2 ? Icons.star : Icons.star_border,
            color: (_iRate >= 2 ? Colors.yellow : Colors.grey),
            size: 15,
          ),
          Icon(
            _iRate >= 3 ? Icons.star : Icons.star_border,
            color: (_iRate >= 3 ? Colors.yellow : Colors.grey),
            size: 15,
          ),
          Icon(
            _iRate >= 4 ? Icons.star : Icons.star_border,
            color: (_iRate >= 4 ? Colors.yellow : Colors.grey),
            size: 15,
          ),
          Icon(
            _iRate >= 5 ? Icons.star : Icons.star_border,
            color: (_iRate >= 5 ? Colors.yellow : Colors.grey),
            size: 15,
          ),
  //          Padding(
  //            padding: const EdgeInsets.all(5.0),
  //          ),
        ],
      ),
    );

    Widget wTrailing = Container ( 
      width: 42,
      child: Icon(
        //Icons.favorite,
        _oWalker.bFavorite ? Icons.favorite : Icons.favorite_border,
        color: Colors.black,
        size: 40,
      ),
    );

    void _onHorizontalDragStartHandler(DragStartDetails details) {
      hInicio = details.globalPosition.dx.floorToDouble();
    }

    void _onDragUpdateHandler(DragUpdateDetails details) {
      hFim = details.globalPosition.dx.floorToDouble();
    }

    return GestureDetector(
      // captura o dx incial do swipe
      onHorizontalDragStart: _onHorizontalDragStartHandler,
      // captura o dx final do swipe
      onHorizontalDragUpdate: _onDragUpdateHandler,
      // captura o momento que o swipe termina
      onHorizontalDragEnd: (details) {
        double result = hFim - hInicio;
        // minimo de 100px para esquerda para remover
        if(result < -100.0) { 
          removeItem(iPos);
        }
        // minimo de 50px para direita para favorita
        else if(result > 50.0) {
          favorita(iPos);
        }
        // print("moveu $result");
      },
      child: Card( 
        color: Colors.white,
        elevation: 2.0,
        child: ListTile(
          leading: wAvatar,
          //title: Expanded (
          title: Container (
            child: Row (
              //crossAxisAlignment: CrossAxisAlignment.start ,
              children: <Widget> [
                wText,
                wIcon,
              ],
            )
          ),
          trailing: wTrailing,
          onTap: () => Navigator.of(context).push(
            MaterialPageRoute(
              builder: (context) => WalkerDetail (
                walker: _oWalker, rate: wIcon
              )
            )
          ),
        ),
      ),
    );
  }


  BottomNavigationBar _showBottomNavigationBar() {
    final List <IconData> oIconList=[Icons.home, Icons.person, Icons.edit_location, Icons.settings];
    final List <String> oHintList=["Home", "Profile", "Address", "Setting"];
    //final List <bool> oEnableList=[true,false,false];
    final List <Color> oColorList=[Colors.grey, Colors.black, Colors.black, Colors.grey];
    
    return BottomNavigationBar(
      currentIndex: 1,
      type: BottomNavigationBarType.fixed,
      showUnselectedLabels: true,
      items: [
        //BottomNavigationBarItem(icon: 
        //  Icon(
        //    oIconList[0],
        //    Color: oEnableList[0],
        //  ),
        //  title: Text(oHintList[0])
        //),
        BottomNavigationBarItem(
          icon: Icon(
            oIconList[0],
            color: oColorList[0],
          ), 
          title: Text(
            oHintList[0]
          )
        ),
        BottomNavigationBarItem(
          icon: Icon(
            oIconList[1],
            color: oColorList[1],
          ), 
          title: Text(
            oHintList[1]
          )
        ),
        //BottomNavigationBarItem(icon: Icon(Icons.person), title: Text('Profile')),
        BottomNavigationBarItem(
          icon: Icon(
            oIconList[2],
            color: oColorList[2],
          ), 
          title: Text(
            oHintList[2]
          )
        ),
        BottomNavigationBarItem(
          icon: Icon(
            oIconList[3],
            color: oColorList[3],
          ), 
          title: Text(
            oHintList[3]
          )
        ),
        /*
        BottomNavigationBarItem(icon: Icon(Icons.home), title: Text('Home')),
        BottomNavigationBarItem(icon: Icon(Icons.person), title: Text('Profile')),
        BottomNavigationBarItem(icon: Icon(Icons.pets), title: Text('Pet')),
        BottomNavigationBarItem(icon: Icon(Icons.favorite_border), title: Text('Favorite')),
        BottomNavigationBarItem(icon: Icon(Icons.search), title: Text('Search')),
        BottomNavigationBarItem(icon: Icon(Icons.settings), title: Text('Settings')),
        BottomNavigationBarItem(icon: Icon(Icons.autorenew), title: Text('Switch'))
        */
        //BottomNavigationBarItem(icon: new Icon(Icons.add_a_photo), title: new Text('Photo')),
        //BottomNavigationBarItem(icon: new Icon(Icons.perm_identity), title: new Text('Profile')),
        //BottomNavigationBarItem(icon: new Icon(Icons.favorite_border), title: new Text('Favorite')),
        //BottomNavigationBarItem(icon: new Icon(Icons.date_range), title: new Text('Schedule')),
        //BottomNavigationBarItem(icon: new Icon(Icons.attach_money), title: new Text('Payment')),
        //BottomNavigationBarItem(icon: new Icon(Icons.payment), title: new Text('Payment')),
        //BottomNavigationBarItem(icon: new Icon(Icons.brightness_medium), title: new Text('Bridghtness')),
        //BottomNavigationBarItem(icon: new Icon(Icons.format_size), title: new Text('FontSize')),
        //BottomNavigationBarItem(icon: new Icon(Icons.power_settings_new), title: new Text('Close')),
      ],
      onTap: (iItem) {
        setState(() {
          if ( (iItem==0) || (iItem==3) ) return;
          Navigator.pushAndRemoveUntil(
            context,
            MaterialPageRoute(
              //builder: (context) => HomeGui()
              builder: (context) {
                if (iItem==1) return UserGui();
                if (iItem==2) return UserAddressGui();
                return PetGui();
              }
            ),
            (Route<dynamic> route) => true,
          );
        });         
      },
    );
  }

} //_WalkerListGuiState
