//https://medium.com/@afegbua/flutter-thursday-13-building-a-UserAddress-registration-and-login-process-with-provider-and-external-api-1bb87811fd1d
//https://bendyworks.com/blog/a-month-of-flutter-UserAddress-registration-form
//https://kodestat.gitbook.io/flutter/22-flutter-checkbox

import 'package:flutter/material.dart';
import 'package:pet/model/ad_address_model.dart';
import 'package:pet/view/master_view.dart';
// import 'package:pet/view/screen/home_gui.dart';
import 'package:pet/view/screen/user_gui.dart';
import 'package:pet/view/screen/pet_gui.dart';

class UserAddressGui extends StatefulWidget {
  @override
  State<StatefulWidget> createState() => new _UserAddressGuiState();
}

class _UserAddressGuiState extends State<UserAddressGui> {
  MasterService _masterService = MasterService.instance;
  final addressController= TextEditingController();
  final numberController= TextEditingController();
  final complementaryController= TextEditingController();
  final neighborhoodController= TextEditingController();
  final cityController= TextEditingController();
  final stateController= TextEditingController(); 
  final sErrorMessage="";

  @override
  void dispose() {
    // Clean up the controller when the widget is disposed.
    addressController.dispose();
    numberController.dispose();
    complementaryController.dispose();
    neighborhoodController.dispose();
    cityController.dispose();
    stateController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {

    return Scaffold(
      
      appBar: AppBar(
        //leading: Icon(Icons.menu),
        leading: new IconButton( 
          icon: new Icon(
            Icons.menu,
            //color: Colors.green[500],
          ),
          onPressed: () {},
        ),    
        title: Row(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget> [
            Icon(
              Icons.pets,
              //color: Colors.green[500],
            ),
            Padding(
              padding: const EdgeInsets.all(10.0),
            ),
            Text (
              "Pet Walking",
            ),
            Padding(
              padding: const EdgeInsets.all(30.0),
            ),
          ],
        ),
      ),

      body: Stack( 
        children: <Widget> [
            _showForm(),
            //_showCircularProgress(),
        ],
      ),

      bottomNavigationBar: _showBottomNavigationBar(),

    );
  }

Widget _showForm() {
  return Container(
    padding: EdgeInsets.all(10.0),
    child: new ListView(
      shrinkWrap: true,
      children: <Widget>[
        //_showLogo(),
        _showTitle(),
        // _showNameInput(),
        _showAddressInput(),
        _showAddressNumberInput(),
        _showNeighborhoodInput(),
        _showCityStateInput(),
        //_showPrimaryButton(),
        //_showSecondaryButton(),
        _showButtons(),
        //_showErrorMessage(),
      ],
    ),
  );
}

Widget _showTitle() {
  return Container(
    alignment: Alignment.center,
    padding: const EdgeInsets.all(20.0),
    child: Text (
      'User\'s Addresss',
      style: Theme.of(context).textTheme.headline6,
    ),
   /* child: Text (
      'User\'s Addresss',
      style: TextStyle(
        color: Colors.blue,
        fontWeight: FontWeight.w500,
        fontSize: 30
      )
    ) 
    */
  );
}

Widget _showAddressInput() {
  return Container(
    alignment: Alignment.center,
    padding: const EdgeInsets.all(10.0),
    child: TextField (
      controller: addressController,
      maxLines:1,
      maxLength: 254,
      keyboardType: TextInputType.text,
      autofocus: false,
      decoration: InputDecoration(
          border: OutlineInputBorder(),
          labelText: "Address",
          hintText: 'Address',
          counterText: "", // Retira o contador
          //icon: new Icon(
          //  Icons.add_location,
          //  color: Colors.grey,
          //)
      ),
      //validator: (value) => value.isEmpty ? 'Email can \'t be empty' : null,
      //onSaved: (value) => sNome = value.trim(),
    )
  );
}

Widget _showAddressNumberInput() {
  return Container(
    alignment: Alignment.center,
    padding: const EdgeInsets.all(10.0),
    child: Row (
      mainAxisAlignment: MainAxisAlignment.center,
      children: <Widget> [
        Expanded (
          child: TextField (
            controller: numberController,
            maxLines:1,
            maxLength: 9,
            keyboardType: TextInputType.text,
            autofocus: false,
            decoration: InputDecoration(
                border: OutlineInputBorder(),
                labelText: "Number",
                hintText: 'Number',
                counterText: "",
                //icon: new Icon(
                //  Icons.home,
                //  color: Colors.grey,
                //)
            ),
            //validator: (value) => value.isEmpty ? 'Email can \'t be empty' : null,
            //onSaved: (value) => sNome = value.trim(),
          ),
        ),
        SizedBox(
          width: 20,
        ),
        //Padding(
        //  padding: const EdgeInsets.all(10.0),
        //),
        Expanded (
          child: TextField (
            controller: complementaryController,
            maxLines:1,
            maxLength: 9,
            keyboardType: TextInputType.text,
            autofocus: false,
            decoration: InputDecoration(
                border: OutlineInputBorder(),
                labelText: "Complementary",
                hintText: "Complementary",
                counterText: "",
                //icon: new Icon(
                //  Icons.add_location,
                //  color: Colors.grey,
                //)
            ),
            //validator: (value) => value.isEmpty ? 'Email can \'t be empty' : null,
            //onSaved: (value) => sNome = value.trim(),
          ),
        ),
      ],
    ),
  );
}

Widget _showNeighborhoodInput() {
  return Container(
    alignment: Alignment.center,
    padding: const EdgeInsets.all(10.0),
    child: TextField (
      controller: neighborhoodController,
      maxLines:1,
      maxLength: 49,
      keyboardType: TextInputType.text,
      autofocus: false,
      decoration: InputDecoration(
          border: OutlineInputBorder(),
          labelText: "Neighborhood",
          hintText: 'Neighborhood',
          counterText: "",
          //icon: new Icon(
          //  Icons.add_location,
          //  color: Colors.grey,
          //)
      ),
      //validator: (value) => value.isEmpty ? 'Email can \'t be empty' : null,
      //onSaved: (value) => sNome = value.trim(),
    )
  );
}

Widget _showCityStateInput() {
  return Container(
    alignment: Alignment.center,
    padding: const EdgeInsets.all(10.0),
    child: Row (
      mainAxisAlignment: MainAxisAlignment.center,
      children: <Widget> [
        Expanded (
          child: TextField (
            controller: cityController,
            maxLines:1,
            maxLength: 49,
            keyboardType: TextInputType.text,
            autofocus: false,
            decoration: InputDecoration(
                border: OutlineInputBorder(),
                labelText: "City",
                hintText: 'City',
                counterText: "",
                //icon: new Icon(
                //  Icons.home,
                //  color: Colors.grey,
                //)
            ),
            //validator: (value) => value.isEmpty ? 'Email can \'t be empty' : null,
            //onSaved: (value) => sNome = value.trim(),
          ),
        ),
        SizedBox(
          width: 20,
        ),
        //Padding(
        //  padding: const EdgeInsets.all(10.0),
        //),
        Expanded (
          child: TextField (
            controller: stateController,
            maxLines:1,
            maxLength: 49,
            keyboardType: TextInputType.text,
            autofocus: false,
            decoration: InputDecoration(
                border: OutlineInputBorder(),
                labelText: "State",
                hintText: "State",
                counterText: "",
                //icon: new Icon(
                //  Icons.add_location,
                //  color: Colors.grey,
                //)
            ),
            //validator: (value) => value.isEmpty ? 'Email can \'t be empty' : null,
            //onSaved: (value) => sNome = value.trim(),
          ),
        ),
      ],
    ),
  );
}

Widget _showButtons() {
  return Container (
    //height: 100,
    //width: 150,
    alignment: Alignment.center,
    padding: EdgeInsets.all(10.0),
    child: Row (
      mainAxisAlignment: MainAxisAlignment.center,
      children: <Widget> [
        Expanded(
          child: SizedBox(
          //child: SizedBox(
            height: 70,
            child: RaisedButton (
              padding: EdgeInsets.all(10.0),
              elevation: 5.0,
              //textColor: Colors.blue,
              color: Colors.red,
              shape: new RoundedRectangleBorder(
                borderRadius: new BorderRadius.circular(50.0),
              ),
              child: new Text(
                'Cancel',
                style: new TextStyle(
                  fontSize: 24.0, 
                  color: Colors.white,
                ),
              ),
              onPressed: () {
                //Show the HomeGui
              },
            ),
          ),
        ),
        SizedBox(
          width: 20,
        ),
        Expanded(
          child: SizedBox(
          //child: SizedBox(
            height: 70,
            child: RaisedButton (
              padding: EdgeInsets.all(10.0),
              elevation: 5.0,
              color: Colors.green,
              shape: new RoundedRectangleBorder(
                borderRadius: new BorderRadius.circular(50.0),
              ),
              child: new Text(
                'Create',
                style: new TextStyle(
                  fontSize: 24.0, 
                  //fontWeight: FontWeight.w500,
                  color: Colors.white,
                ),
              ),
              onPressed: () {
                //Show the HomeGui
                if(addressController.text.isEmpty ||
                  numberController.text.isEmpty ||
                  // complementaryController.text.isEmpty ||
                  neighborhoodController.text.isEmpty ||
                  cityController.text.isEmpty ||
                  stateController.text.isEmpty) {
                    return _invalidDialog(" Ha campos vazios.");
                  }
                  doAddressCreate();
              },
            ),
          ),
        ),
      ],
    ),
  );
}

  void doAddressCreate() async {
    bool result = await this._masterService.doRegisterAddress(AddressModel(
      address: addressController.text,
      city: cityController.text,
      neighborhood: neighborhoodController.text,
      state: stateController.text,
    ), number: numberController.text, complementary: complementaryController.text);
    if(result == false) {
      return _invalidDialog("Erro durante o armazenamento do endereco.\n");
    }
    // Muda para a pagina de cadastro do endereco
    Navigator.pushAndRemoveUntil(context,
      MaterialPageRoute(builder: (context) => PetGui()),
      (Route<dynamic> route) => false,
    );
  }

  Future _invalidDialog(String message) {
    return showDialog(
      context: context,
      builder: (context) {
        return AlertDialog(
          content: Text(message, textAlign: TextAlign.center, style: new TextStyle(fontSize: 20.0,
          ),),
        );
      },
    );
  }


BottomNavigationBar _showBottomNavigationBar() {
  final List <IconData> oIconList=[Icons.home, Icons.person, Icons.pets, Icons.settings];
  final List <String> oHintList=["Home", "Profile", "Pet", "Setting"];
  //final List <bool> oEnableList=[true,false,false];
  final List <Color> oColorList=[Colors.grey, Colors.black, Colors.black, Colors.grey];
  
  return BottomNavigationBar(
    currentIndex: 1,
    type: BottomNavigationBarType.fixed,
    showUnselectedLabels: true,
    items: [
      //BottomNavigationBarItem(icon: 
      //  Icon(
      //    oIconList[0],
      //    Color: oEnableList[0],
      //  ),
      //  title: Text(oHintList[0])
      //),
      BottomNavigationBarItem(
        icon: Icon(
          oIconList[0],
          color: oColorList[0],
        ), 
        title: Text(
          oHintList[0]
        )
      ),
      BottomNavigationBarItem(
        icon: Icon(
          oIconList[1],
          color: oColorList[1],
        ), 
        title: Text(
          oHintList[1]
        )
      ),
      //BottomNavigationBarItem(icon: Icon(Icons.person), title: Text('Profile')),
      BottomNavigationBarItem(
        icon: Icon(
          oIconList[2],
          color: oColorList[2],
        ), 
        title: Text(
          oHintList[2]
        )
      ),
      BottomNavigationBarItem(
        icon: Icon(
          oIconList[3],
          color: oColorList[3],
        ), 
        title: Text(
          oHintList[3]
        )
      ),
      /*
      BottomNavigationBarItem(icon: Icon(Icons.home), title: Text('Home')),
      BottomNavigationBarItem(icon: Icon(Icons.person), title: Text('Profile')),
      BottomNavigationBarItem(icon: Icon(Icons.pets), title: Text('Pet')),
      BottomNavigationBarItem(icon: Icon(Icons.favorite_border), title: Text('Favorite')),
      BottomNavigationBarItem(icon: Icon(Icons.search), title: Text('Search')),
      BottomNavigationBarItem(icon: Icon(Icons.settings), title: Text('Settings')),
      BottomNavigationBarItem(icon: Icon(Icons.autorenew), title: Text('Switch'))
      */
      //BottomNavigationBarItem(icon: new Icon(Icons.add_a_photo), title: new Text('Photo')),
      //BottomNavigationBarItem(icon: new Icon(Icons.perm_identity), title: new Text('Profile')),
      //BottomNavigationBarItem(icon: new Icon(Icons.favorite_border), title: new Text('Favorite')),
      //BottomNavigationBarItem(icon: new Icon(Icons.date_range), title: new Text('Schedule')),
      //BottomNavigationBarItem(icon: new Icon(Icons.attach_money), title: new Text('Payment')),
      //BottomNavigationBarItem(icon: new Icon(Icons.payment), title: new Text('Payment')),
      //BottomNavigationBarItem(icon: new Icon(Icons.brightness_medium), title: new Text('Bridghtness')),
      //BottomNavigationBarItem(icon: new Icon(Icons.format_size), title: new Text('FontSize')),
      //BottomNavigationBarItem(icon: new Icon(Icons.power_settings_new), title: new Text('Close')),
    ],
    onTap: (iItem) {
      setState(() {
        if ( (iItem==0) || (iItem==3) ) return;
        Navigator.pushAndRemoveUntil(
          context,
          MaterialPageRoute(
            //builder: (context) => HomeGui()
            builder: (context) {
              if (iItem==1) return UserGui();
              if (iItem==2) return PetGui();
              return UserAddressGui();
            }
          ),
          (Route<dynamic> route) => true,
        );
      });         
    },
  );
}

}
