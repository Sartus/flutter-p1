//Source: https://github.com/tattwei46/flutter_login_demo/blob/master/lib/pages/login_signup_page.dart
//https://medium.com/@afegbua/flutter-thursday-13-building-a-user-registration-and-login-process-with-provider-and-external-api-1bb87811fd1d
//https://stackoverflow.com/questions/61534182/user-registration-and-authentication-in-flutter
//https://medium.com/@afegbua/flutter-thursday-13-building-a-user-registration-and-login-process-with-provider-and-external-api-1bb87811fd1d
//https://medium.com/flutter-comunidade-br/criando-um-aplicativo-para-lembrar-dos-objetos-emprestados-com-flutter-e-sembast-6dbf350a7a56

import 'package:flutter/material.dart';
import 'package:pet/model/us_user_model.dart';
import 'package:pet/view/master_view.dart';
// import 'package:pet/view/screen/home_gui.dart';
import 'package:pet/view/screen/user_gui.dart';
import 'package:pet/view/screen/walker_list_gui.dart';

class LoginGui extends StatefulWidget {
  @override
  //LoginGuiState createState() => new LoginGuiState();
  State<StatefulWidget> createState() => new _State();
}

//class LoginGuiState extends State<LoginGui> {
class _State extends State<LoginGui> {
  MasterService _masterService = MasterService.instance;
  //final formKey = new GlobalKey<FormState>();
  final nameController = TextEditingController();
  final passController = TextEditingController();
  String sErrorMessage="";
  //bool bIsLoginForm;
  //bool bIsLoading;

  @override
  void dispose() {
    // Clean up the controller when the widget is disposed.
    nameController.dispose();
    passController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {

    return Scaffold(
      appBar: _appBar(),

      body: Stack( 
        children: <Widget> [
          _showForm(),
          //_showCircularProgress(),
        ],
        //),
      ),
    );
  }

PreferredSizeWidget _appBar() {
  return AppBar(
    title: Row(
      mainAxisAlignment: MainAxisAlignment.center,
      children: <Widget> [
        Icon(
          Icons.pets,
          //color: Colors.green[500],
        ),
        Padding(
          padding: const EdgeInsets.all(10.0),
        ),
        Text (
          "Pet Walking",
        ),
        //Padding(
        //  padding: const EdgeInsets.all(30.0),
        //),
      ],
    ),
  );
}

Widget _showForm() {
  return Container(
    padding: EdgeInsets.all(10.0),
    child: new ListView(
    //child: new Form(
      //key: _formKey,
      shrinkWrap: true,
      children: <Widget>[
        _showLogo(),
        _showTitle(),
        _showNameInput(),
        _showPassInput(),
        _showPrimaryButton(),
        _showSecondaryButton(),
        //_showErrorMessage(),
      ],
    ),
  );
}

Widget _showLogo() {
  return Container(
    alignment: Alignment.center,
    padding: const EdgeInsets.all(20.0),
    child: Image(
      height: 100,
      width: 100,
      image: AssetImage('assets/icons/pet_icon_transparent_yes.png')
    ) 
  );
}

Widget _showTitle() {
  return Container(
    alignment: Alignment.center,
    padding: const EdgeInsets.all(20.0),
    child: Text (
      'Login',
      style: TextStyle(
        color: Colors.blue,
        fontWeight: FontWeight.w500,
        fontSize: 30
      )
    ) 
  );
}

Widget _showNameInput() {
  return Container(
    alignment: Alignment.center,
    padding: const EdgeInsets.all(10.0),
    child: TextField (
      controller: nameController,
      maxLines:1,
      maxLength: 59,
      keyboardType: TextInputType.text,
      autofocus: false,
      decoration: InputDecoration(
          border: OutlineInputBorder(),
          labelText: "User Name",
          hintText: 'Identification',
          counterText: "",
          icon: new Icon(
            Icons.person,
            color: Colors.grey,
          )
      ),
      //validator: (value) => value.isEmpty ? 'Email can \'t be empty' : null,
      //onSaved: (value) => sNome = value.trim(),
    )
  );
}

Widget _showPassInput() {
  return Container(
    alignment: Alignment.center,
    padding: const EdgeInsets.all(10.0),
    child: TextField (
      controller: passController,
      maxLines:1,
      maxLength: 17,
      obscureText: true,
      autofocus: false,
      keyboardType: TextInputType.text,
      decoration: InputDecoration(
          border: OutlineInputBorder(),
          labelText: "Password",
          hintText: 'Password',
          counterText: "",
          icon: new Icon(
            Icons.lock,
            color: Colors.grey,
          )
      ),
      //validator: (value) => value.isEmpty ? 'Password can \'t be empty' : null,
      //onSaved: (value) => sPass = value.trim(),
    )
  );
}

Widget _showSecondaryButton() {
  return Container (
    child: Row(
      mainAxisAlignment: MainAxisAlignment.center,
      children: <Widget> [
        FlatButton(
          textColor: Colors.grey,
          child: Text (
            'Forgot Password',
            style: TextStyle(
              fontSize: 16.0, 
            ),
          ),
          //onPressed: toggleFormMode,
          onPressed: () {
            //Show the RecorveGui
          },
        ),
        //Padding(
        //  padding: const EdgeInsets.all(10.0),
        //),
        Text (
          '|',
          style: TextStyle(
            fontSize: 16.0, 
          ),
        ),
        //Padding(
        //  padding: const EdgeInsets.all(10.0),
        //),
        FlatButton(
          textColor: Colors.blue,
          child: Text (
            'Register',
            style: TextStyle(
              fontSize: 16.0, 
            ),
          ),
          //onPressed: toggleFormMode,
          onPressed: () {
            //Show the RecorveGui
            Navigator.push(
              context,
              MaterialPageRoute(builder: (context) => UserGui()),
            );
          },
        ),
      ],
    )
  );
}

Widget _showPrimaryButton() {
  return Container (
    alignment: Alignment.center,
    padding: EdgeInsets.all(10.0),
    child: SizedBox(
      height: 70,
      width: 200,

      child: RaisedButton (
        padding: EdgeInsets.all(10.0),
        elevation: 5.0,
        color: Colors.green,
        shape: new RoundedRectangleBorder(
          borderRadius: new BorderRadius.circular(50.0),
        ),
        child: new Text(
          'Enter',
          style: new TextStyle(
            fontSize: 24.0, 
            //fontWeight: FontWeight.w300,
            color: Colors.white,
          ),
        ),
        onPressed: () {
          doLogin();
        },
      ),
    ),
  );
}

void doLogin() async {
  bool result = await this._masterService.doLogin(UserModel(userName: nameController.text,
  userPwd: passController.text));
  print(passController.text == null);
  if(result == false) {
    return _invalidDialog("Nome ou senha incorreto.");
  }
  // Muda para a pagina de cadastro do endereco
    Navigator.pushAndRemoveUntil(context,
      MaterialPageRoute(builder: (context) => WalkerListGui()),
      (Route<dynamic> route) => false,
    );
}

Future _invalidDialog(String message) {
  return showDialog(
    context: context,
    builder: (context) {
      return AlertDialog(
        content: Text(message, textAlign: TextAlign.center,
        style: new TextStyle(fontSize: 20.0),
        ),
      );
    },
  );
}

}
