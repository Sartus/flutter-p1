

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:pet/view/screen/walker_pet_list_gui.dart';
import 'package:pet/view/screen/walker_list_gui.dart';

class WalkerDetail extends StatelessWidget {
  final Walker _walker;
  final Widget rate;

  WalkerDetail({@required walker, @required this.rate}) : _walker = walker;

  @override
  Widget build(BuildContext context) {
    return 
      Scaffold(
        appBar: AppBar(
          title: Text("Walker Details"),
        ),
        body: SingleChildScrollView(
          child: Padding(
            padding: const EdgeInsets.all(12.0),
            child: Column(
              children: <Widget>[
                Card(
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: <Widget>[
                      ListTile(
                        title: Text("Name",
                          style: TextStyle(fontSize: 20.0,)
                        ),
                        subtitle:
                          GestureDetector(
                            onTap: () => Navigator.of(context).push(
                              MaterialPageRoute(
                                builder: (context) => WalkerPetListGui(_walker),
                              ),
                            ),
                            child: Text("${_walker.sName}",
                            style: TextStyle(fontSize: 20.0,)
                          ),
                        ),
                      ),
                      ListTile(
                        title: Text("Address",
                          style: TextStyle(fontSize: 20.0,)
                        ),
                        subtitle: Text("${_walker.sAddress}",
                          style: TextStyle(fontSize: 20.0,)
                        ),
                      ),
                      ListTile(
                        title: Text("Rate"),
                        subtitle: Row(children: <Widget> [rate]),
                      ),
                    ],
                  ),
                ),
              ],
            ),
          ),
        ),
      );
  }
}