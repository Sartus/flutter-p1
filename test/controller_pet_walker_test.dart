

import 'package:flutter_test/flutter_test.dart';
import 'package:pet/common/commmon.dart';
import 'package:pet/controller/pet_walker_controller.dart';
import 'package:pet/db/server_conn_db.dart';
import 'package:pet/model/pet_walker_model.dart';

final PetWalkerController  ctr = PetWalkerController(type: EnumSideType.server);

void main() {
  TestWidgetsFlutterBinding.ensureInitialized();
  tearDownAll(() async {
    // limpa o banco apos todos os testes.
    var db = ServerDatabase.instance;
    db.deleteDb();
  });
  group("ControllerPetWalker tests", () {
    test("Sucesso: Busca pele linha que contem id=1, busca por todas as colunas," +
      " busca por um numero limitado de colunas." +
      "Entidade: ControllerPetWalker\n" +
      "Condição de Teste: “retorno != null” " +
      "Resultado Esperado: “All tests passed” ",
      () async {
      PetWalkerModel resultgetById = await ctr.getById(1);
      expect(resultgetById != null, true);

      List<PetWalkerModel> resultgetAllRows = await ctr.getAllRows();
      expect(resultgetAllRows.isNotEmpty, true);

      List<PetWalkerModel> resultgetRows = await ctr.getRows();
      expect(resultgetRows.length == 10, true);
    });
    test("\Erro : Busca por id (9876) inexistente.\n" + 
      "Entidade: ControllerPetWalker\n" +
      "Condição de Teste: “obj == null”\n" +
      "Resultado Esperado: “All tests passed”\n", 
    () async {
      PetWalkerModel resultgetByIdError = await ctr.getById(9876);
      expect(resultgetByIdError == null, true);
    });
    test("Sucesso: Insert de linha vazia na tabela, " + 
      "insert de linha com id=9876, update na linha com id=9876." + 
      "Entidade: ControllerPetWalker\n" +
      "Condição de Teste: “retorno == id” " +
      "Resultado Esperado: “All tests passed” ",
      () async {
      int resultInsertVazio = await ctr.insert(PetWalkerModel());
      expect(resultInsertVazio > 0, true);

      int resultInsertObj = await ctr.insert(PetWalkerModel(idPetWalker: 9876));
      expect(resultInsertObj == 9876, true);

      int resultUpdate = await ctr.update(PetWalkerModel(idPetWalker: 9876));
      expect(resultUpdate == 1, true);
    });
    test("\nERRO : Busca por id inexistente, Insert id existente, Update id inexistente.\n" + 
      "Entidade: ControllerPetWalker\n" +
      "Condição de Teste: “obj == null”\n" +
      "Resultado Esperado: “All tests passed”\n", 
    () async {
      PetWalkerModel resultgetByIdError = await ctr.getById(0);
      expect(resultgetByIdError == null, true);
      int resultInsertObj;
      
      resultInsertObj = await ctr.insert(PetWalkerModel(idPetWalker: 9876));
      
      expect(resultInsertObj == 0, true);

      int resultUpdate = await ctr.update(PetWalkerModel(idPetWalker: 48452));
      expect(resultUpdate == 0, true);
    });
    test("Sucesso: Delete em ID(9876) existente" + 
      "Entidade: ControllerPetWalker\n" +
      "Condição de Teste: “retorno == 1” " +
      "Resultado Esperado: “All tests passed” ",
      () async {
      int resultDelete = await ctr.delete(9876);
      expect(resultDelete == 1, true);
    });
    test("Erro: Delete em ID(9876) inexistente" + 
      "Entidade: ControllerPetWalker\n" +
      "Condição de Teste: “retorno == 0” " +
      "Resultado Esperado: “All tests passed” ",
      () async {
      int resultDelete = await ctr.delete(9876);
      expect(resultDelete == 0, true);
    });
  });
}